/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualkeyboard;
import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author Kruti
 */
public class VirtualKeyBoard extends JPanel implements ActionListener{

    /**
     * @param args the command line arguments
     */
    private static final long serialVersionUID = 1L;
    static final String st = "abcdefghijklmnopqrstuvwxyz";
    JButton buttonList[];
    String buffer = "\n";
    JTextField text;
    JButton capsLock;
    boolean capslock;
    
    public VirtualKeyBoard ()
    {
        JPanel pane = new JPanel();
        add(pane);
    }

    public void key() {
        text = new JTextField(20);
        text.setActionCommand(""+ buffer);
        add(text);
        int n = st.length();
        buttonList = new JButton[n];
        for (int i = 0; i < n; i++) {
            buttonList[i] = new JButton( "" + st.charAt(i) );
            add(buttonList[i]);
            buttonList[i].addActionListener(this);
        }
        capsLock = new JButton("CapsLock Key");
        add(capsLock);
        capsLock.addActionListener(this);
    }
    public void actionPerformed( ActionEvent e) {
        int n = st.length();
        if (e.getSource() == capsLock)
        {
            if (capslock)
                capslock = false;
            else capslock = true;
        }
        else{
            for (int i = 0; i < n; i++)
            {
                if (e.getSource() == buttonList[i])
                {
                    if (capslock)
                    {
                        buffer += st.charAt(i);
                    }
                    else
                    {
                        buffer += st.toLowerCase().charAt(i);
                    }
                    text.setText(""+ buffer);
                    break;
                }
            }
        }
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        VirtualKeyBoard keys= new VirtualKeyBoard();
        frame.getContentPane().add(keys,"Center");
        keys.key();
        frame.setSize(500,200);
        frame.setVisible(true);
    }
    
}



